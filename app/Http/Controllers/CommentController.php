<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allComments = Comment::all();

        function replies($comment_id)
        {
            $replies = Comment::where('reply_to', '=', $comment_id)->get();
            return $replies;
        }

        function SubComments($replies)
        {
            foreach ($replies as $reply)
            {
                $replies = replies($reply->id);
                $reply->reply_comment = $replies;
                SubComments($replies);
            }
        }

        function hasParent($comment){
            if ($comment->reply_to !== '0')
            {
                return false;
            }
            else{
                return true;
            }
        }

        $arrayComments = array();

        foreach ($allComments as $comment)
        {
            $replies = replies($comment->id);
            $comment->reply_comment = $replies;
            SubComments($replies);

            if(!hasParent($comment))
            {
                array_push($arrayComments, $comment);
            }

        }

        $array = array(
            "success" => true,
            "data" => $arrayComments
        );

        $data = json_encode($array,JSON_PRETTY_PRINT);

        return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->isJson()){

            $request->validate([
                'content' => 'required|max:255'
            ]);

            $comment = Comment::create([
                'content' => $request->content,
                'reply_to' => 0
                ]);

            return response()->json([
                'success' => true,
                'message' => 'komentarz dodany'

            ], 201);

        } else{
            return response()->json([
                'success' => false,
                'message' => 'nieprawidłowy format'

            ], 400);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        $array = array(
            "success" => true,
            "data" => Comment::find($comment)
        );

        $data = json_encode($array, JSON_PRETTY_PRINT);

        return $data;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {

        $comment->content = $request->content;
        $comment->reply_to = $request->reply_to;
        $comment->save();

        return response()->json([
            'success' => true,
            'message' => 'komentarz zmieniony'

        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return response()->json([
            'success' => true,
            'message' => 'komentarz usunięty'

        ], 200);
    }

    public function View()
    {
        return view('comments')
            ->with('comments', Comment::all());
    }
}

Methods:

comments.index
Shows list **of all** comments
Example Request

GET /api/json/comments/
Content-Type application/json
Accept application/json
_____________________________

comments.index
Shows **one** comment
Example Request

GET /api/json/comments/1
Content-Type application/json
Accept application/json
_____________________________

comments.store
**Stores** comment to the database
Example Request

POST /api/json/comments/
Content-Type application/json
Accept application/json
{
  "content": "Komentarz",
  "reply_to": "0"
  }
_____________________________

comments.update
**Updates** Comment
Example Request

PUT /api/json/comments/11
Content-Type application/json
Accept application/json
{
  "content": "Nowy komentarz",
  "reply_to": "0"
  }
_____________________________

comments.delete
**Deletes** Comment
Example Request

DELETE /api/json/comments/1
Content-Type application/json
Accept application/json
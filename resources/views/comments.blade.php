<!doctype html>
<html lang="pl">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Komentarze!</title>
</head>
<body>


<div class="container">

    <div class="card mt-5 text-white bg-secondary mb-3">
        <div class="card-header text-center"><h1>Komentarze</h1></div>
        <ol>
            @foreach($comments as $comment)
            <li>
        <div class="card-body">
            <p class="card-text">{{ $comment->content }}</p>
        </div>
            </li>
                @endforeach
        </ol>


    </div>

</div>

<script src="{{asset ('js/app.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script>
    $json = axios.get('http://comments-inpay.loc/api/json/comments/');
    console.log($json);
</script>
</body>
</html>

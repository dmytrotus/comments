Metody:
comments.index
Pokazuje liste wszystkich komentarzy
Przykład zapytania

GET http://comments-inpay.loc/api/json/comments/
Content-Type application/json
Accept application/json

comments.index
Pokazuje jeden komentarz
Przykład zapytania

GET http://comments-inpay.loc/api/json/comments/1
Content-Type application/json
Accept application/json

comments.store
zapisuje komentarz w Bazę danych
Przykład zapytania

POST http://comments-inpay.loc/api/json/comments/
Content-Type application/json
Accept application/json
{
  "content": "Komentarz",
  "reply_to": "0"
  }

comments.update
aktualizuje komentarz w Bazie danych
Przykład zapytania

PUT http://comments-inpay.loc/api/json/comments/11
Content-Type application/json
Accept application/json
{
  "content": "Nowy komentarz",
  "reply_to": "0"
  }

comments.delete
Usuwa komentarz
Przykład zapytania

DELETE http://comments-inpay.loc/api/json/comments/1
Content-Type application/json
Accept application/json
